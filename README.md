# Data_Structures
Practicing data structures for my university course 

This is a repository with all my exercises! You can copy them if you like but don't forget to mention my name! :-)

Data Structures lesson from Alexander Tecnical Institute.
Copyright (C) 2015  George Politis

This program is free software; you can redistribute it and
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
