//Ex.1.1-1.3 Data Structures, George Politis
import java.util.Scanner;
public class Lab
{
    // dimiourgia pinaka lab1 gia tin eisagogi ton foititon
    Student lab1[];
	private int counter=-1;
	public Lab()
    {
        // endeiktiko megethos pinaka anti gia 25
        lab1= new Student[3];
	}

	// eisagogi foititi
	void insertStudent(Student s)
    {
        // gia kathe Student auksanontai oi theseis tou pinaka
		lab1[++counter] = s;
	}
	
	// diagrafi foititi
    void deleteStudent(int arMitrwou)
    {
		for(int i = 0; i<=counter; i++)
        {
			if(lab1[i].getArithmos_mitr()==arMitrwou)
            {
				lab1[i]=null;
			}
		}
	}// end delete student

    // anazitisi foititi typou Student afou epistrefei antikeimeno typou Student
    Boolean findStudent(int arMitrwou)
    {
        boolean flag=false;
        for(int i = 0; i<=counter; i++)
        {
            if(lab1[i].getArithmos_mitr()==arMitrwou)
            {
                flag=true;
                break;
            }
        }
    return flag;
    }// end findStudent

    // Ektyposi parousiologiou gia to tmima
	public void printLab()
    {
       for(int i = 0;i<=counter;i++)
		System.out.println(lab1[i]);
	}// end print method

	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		Lab lab1 = new Lab();
		for (int i=0; i<3; i++)
		{
            // arxikopoiisi antikeimenou kai eisagogi stoixeion
			Student student = new Student();
			System.out.print("Dwse arithmo Mitrwou:");
			student.setArithmos_mitr(in.nextInt());
			System.out.print("Dwse onoma foititi");
            student.setName(in.next());
            System.out.print("Dwse epitheto foititi");
            student.setSurname(in.next());
            System.out.print("Dwse Ilikeia foititi");
            student.setAge(in.nextDouble());
            System.out.print("Dwse filo foititi");
            // kano auto to trick epeidi i Scanner class einai sxediasmeni na diavazei
            // lekseis xorismenes me kena kai oxi enan-enan haraktira
            // pairno dld apo to epomeno token ton proto haraktira
            student.setSex(in.next().charAt(0));
            System.out.print("Dwse arithmoapousiwn sto mathima");
            student.setApousies(in.nextInt());
            System.out.print("Dwse vathmo");
            student.setVathmos(in.nextInt());


			lab1.insertStudent(student);
		}
	lab1.printLab();
	lab1.deleteStudent(12);
	lab1.printLab();
	}// end main
}// end class Lab
