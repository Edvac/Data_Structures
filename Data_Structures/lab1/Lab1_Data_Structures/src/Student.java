import java.lang.Override;
import java.lang.String;
import java.util.Scanner;

//lab1, Domes, George Politis
// Exercise 1.1-1.3
public class Student {
	// attributes
	private int arithmos_mitr;
	private String name;
	private String surname;
	private Double age;
	char  sex;
	int apousies;
	double vathmos;

    // default constructor
    public Student(){};
    // main constructor
    public Student(int arithmos_mitr, String name, String surname, Double age, char sex, int apousies, double vathmos) {
        this.arithmos_mitr = arithmos_mitr;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
        this.apousies = apousies;

        this.vathmos = vathmos;
    }

    // Getter n Setter methods
    public int getArithmos_mitr() {
        return arithmos_mitr;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Double getAge() {
        return age;
    }

    public char getSex() {
        return sex;
    }

    public int getApousies() {
        return apousies;
    }

    public double getVathmos() {
        return vathmos;
    }

    public void setVathmos(double vathmos) {
        this.vathmos = vathmos;
    }

    public void setArithmos_mitr(int arithmos_mitr) {
        this.arithmos_mitr = arithmos_mitr;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void setApousies(int apousies) {
        this.apousies = apousies;
    }

    // toString method , eukoloteri ektiposi tou antikeimenou Student
    @Override
    public String toString() {
        return "Student{" +
                "arithmos_mitr=" + arithmos_mitr +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", apousies=" + apousies +
                ", vathmos=" + vathmos +
                '}';
    }

    public static void main(String[] args) {


		Student s= new  Student(11,"george","politis",23.00,'M',0,8.0);
        System.out.println(s);



    }// end main
}// end class Student
